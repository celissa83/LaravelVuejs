<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{
    public function store()
    {
        request()->validate([
            'content' => ['required'],
        ]);

        User::create([
            'name' => request('content'),
            "email" => 'laeti@test.fr',
            'password' => 'test',
        ]);

        return 'ok';
    }
}
