<?php

namespace App\Http\Controllers;

use App\Comments;

class CommentsController extends Controller
{
    public function index()
    {
        $comments = Comments::whereNull('respond_to_id')

            //->get();
            ->paginate(3);
        return $comments;
    }
    public function store()
    {
        request()->validate([
            'body' => ['required'],
            'name' => ['required'],
        ]);
        return Comments::create([
            'respond_to_id' => request('respond_to_id'),
            'name' => request('name'),
            'url' => 'url',
            "body" => request('body'),
        ])->setRelation('children', collect());
    }
}
