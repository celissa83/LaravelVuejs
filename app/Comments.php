<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = ['name', 'url', 'body','respond_to_id'];

    protected $with = ['children'];
    
    public function children(){
        return $this->hasMany(Comments::class, 'respond_to_id');
    }
}
