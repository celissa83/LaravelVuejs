require('./bootstrap');

import Vue from 'vue';
//import Vuetify from '../plugins/vuetify';
import Vuetify from 'vuetify';
import App from './views/App';
import Routes from '@/js/route.js'
Vue.use(Vuetify);
import 'vuetify/lib'
const app = new Vue({
    vuetify: new Vuetify({
        theme: { dark: true },
    }),

    el: '#app',
    router: Routes,
    render: h => h(App),
})

export default app;

/*new Vue({
    Routes,

    render: h => h(App)
}).$mount('#app')*/