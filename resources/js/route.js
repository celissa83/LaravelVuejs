import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [{
            path: '/news',
            name: "news",
            component: () =>
                import ('./components/dashboard/news')
        },
        {
            path: "/profil",
            name: "profil",
            component: () =>
                import ('./components/profil')
        },
        {
            path: "/comments",
            name: "comments",
            component: () =>
                import ('./components/comments')
        },
        {
            path: "/statistiques",
            name: "statistiques",
            component: () =>
                import ('./components/dashboard/statistiques')
        },
        {
            path: "/",
            name: "",
            component: () =>
                import ('./components/dashboard/agenda')
        },
        {
            path: '/pageafaire',
            name: "pageafaire",
            component: () =>
                import ('./components/pageafaire')
        },
        {
            path: '/clients',
            name: "clients",
            component: () =>
                import ('./components/documents/clients'),
            children: [{
                props: true,
                path: "/panelClient/:id",
                name: "panelClient",
                component: () =>
                    import ('./components/documents/panelDroite')
            }, ]

        },
    ]


})

export default router;